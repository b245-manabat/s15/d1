// [Section] Comments

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe a written code.

// There are two types of comments:
// - Single line comment (ctrl + /) denoted by two slashes (//)
/*
	- Multiline comment
	- denoted by / * * /
	- ctrl + shift + /
*/

// [SECTION] Statements and Syntax
	//  Statements
		// In programming language, statements are instructions that we tell the computer to perform
		// JS statements usually ends with semicolom (;).
			// (;) is also called as delimiter
		//  Semicolons are not required.
			// it is used to help us train or locate where a statement ends.
		// Syntax
			// In programming, it is the set of rules that describes how statements must be constructed.

console.log("Hello World");

// [SECTION] Variables

	//  It is used to contain data.

	// Declaring a variable
		// tells our devices that a variable name is created and is ready to store data.

		// Syntax: let/const variableName;

		// let is a keyword that is usually used in declaring a variable.
	let myVariable;
	console.log(myVariable); // useful for pringitng values of a variable

	//	console.log(Hello World); // error: not found

		// Initialize a value
			// Storing the initial value of a variable
			// Assignment Operator (=)
	myVariable = "Hello";

	// Reassignment a variable value
		// Changing the initial value to a new value
	myVariable = "Hellow World";
	console.log(myVariable);

		// const
			// "const" keyword is used tp declase and initialized a constant variable.
			// A "constant" variable does not change.
	// const PI;
	// PI = 3.1416;
	// console.log(PI);

// Declaring with initialization
	// a variable is given it's initial/starting value upon declaration.
		// Syntax: let/const variableName = value;

		let	productName = "Desktop Compute";
		console.log(productName);

		let productPrice = 18999;
		console.log(productPrice);

		// const keyword
		const PI = 3.1416;
		console.log(PI);

/*
	Guide in writing variables.
	1. Use the "let" keyword if the variable will contain dfferent values/can change over time.
		Naming convention: camelCasing - variable name should starts with "lowercase characters" and "camelCasing" is use for multiple words.
	2. use the "const" keyword if the variable does not changed.
		Naming convention: "const" keyword should be followed by ALL CAPITAL VARIABLE NAME (single valeu variable).
	3. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

	4. Variable name are case sensitive.
*/

// Multiple variable declaration

		let	productCode = "DC017", productBrand = "Dell";
		console.log(productCode, productBrand);

// Using a reserve keyword


// [SECTION] Data Types

// In JavaScript, there are six types of data

	// String
		// are series of characters that create a word, a phrase, a sentence or anything related to creating text.
		// Strings in JavaScript can be written using a single quote ('') or double qoute ("")

		let	country = "Philippines";
		let province = 'Metro Manila';

		// Concatinating Strings
		// Multiple string values that can be combined to create a single String using the "+" symbol.

		// I live in Metro Manila, Philppines
		let	greeting = "I live in " + province + ", " + country;
		console.log(greeting);

		// Escape Characters
		// (\) in string combination with other characters can produce different result
			// "\n" refers to creating a new line.
		let mailAddress = "Quezon City\nPhilippines";
		console.log(mailAddress);

		// Expected output: John's employees went home early.
		let message = 'John\'s employees went home early.';
		console.log(message);

	// Numbers

		// includes positive, negative, or numbers with decimal places
		// Integers/Whole Numbers
		let headcount = 26;
		console.log(headcount);

		// Decimal Numbers/Fractions
		let grade = 98.7;
		console.log(grade);

		// Exponential Notation
		let planetDistance = 2e10;
		console.log(planetDistance);

		// Combine text and strings
		console.log("John's grade last quarter is "+grade);

	// Boolean
		// Boolean values are normally used to store values relating to the state of certain things.
		// true or false (two logical value)

		let	isMarried = false;
		let	isGoodConduct = true;

		console.log("isMarried: "+isMarried);
		console.log("isGoodConduct: "+isGoodConduct);

	// Arrays
		// are special kind of data type that can used to store multiple related values.
	// Syntax: let/const arrayName = [elementA, elementB, ... elementNth];

		let	grades = [98.7, 92.1, 90.2, 94.6];
		console.log(grades);

		let	detais = ["John", "Smith", 32, true];
		console.log(details);

	// Objects
		// Objects are another special kind of data type that is used to mimic real world objects/items.
		// used to create complex data that contains information relevant to each other.